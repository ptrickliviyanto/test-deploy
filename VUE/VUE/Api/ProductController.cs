﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VUE.Services;
using VUE.ViewModels;

namespace VUE.Api
{
    [Route("api/v1/product")]
    [ApiController]
    public class ProductController : Controller
    {
        private readonly ProductService _ProductMan;

        public ProductController(ProductService productService)
        {
            this._ProductMan = productService;
        }

        // GET: api/Product
        [HttpGet("GetAllProduct",Name = "GetAllProduct")]
        public async Task<ActionResult<List<ProductViewModel>>> GetAllProductAsync()
        {
            var allData = await this._ProductMan.GetAllProduct();
            return Ok(allData);
        }

        [HttpGet("GetProductbyId/{id}", Name = "GetProductbyId")]
        public async Task<ActionResult<ProductViewModel>> GetProductbyIdAsync(int id)
        {
            var Data = await this._ProductMan.GetProductbyId(id);
            return Ok(Data);
        }

        [HttpPost("InsertProduct",Name = "InsertProduct")]
        public async Task<ActionResult<List<ProductViewModel>>> UpdateProductAsync(ProductInsertViewModel parameterProduct)
        {
            await this._ProductMan.InsertProduct(parameterProduct);
            return Ok();
        }

        [HttpPost("UpdateProduct",Name = "UpdateProduct")]
        public async Task<ActionResult> InsertProductAsync(int id, [FromBody] ProductInsertViewModel parameterProduct)
        {
            await this._ProductMan.UpdateProduct(id, parameterProduct);
            return Ok();
        }
    }
}
