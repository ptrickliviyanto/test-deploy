﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VUE.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VUE.Api
{
    [Route("api/v1/image")]
    public class ImageController : Controller
    {
        private readonly ProductService _ProductMan;

        public ImageController(ProductService productService)
        {
            this._ProductMan = productService;
        }

        // GET api/<controller>/5
        [HttpGet("{id}",Name ="GetImage")]
        public async Task<ActionResult> GetImageMinioAsync(Guid id)
        {
            var url = await this._ProductMan.GetMinioImage(id);
            return Redirect(url);
        }
     }
}
