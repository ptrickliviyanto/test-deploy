﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VUE.Services;
using VUE.ViewModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VUE.Api
{
    [Route("api/v1/customer")]
    public class CustomerController : Controller
    {
        private readonly CustomerService _CustomerMan;

        public CustomerController(CustomerService customerService)
        {
            this._CustomerMan = customerService;
        }
        // POST api/<controller>
        [HttpPost("RegisterCustomer", Name = "RegisterCustomer")]
        public async Task<IActionResult> RegisterCustomerAsync([FromBody]CustomerViewModel parameterCustomer)
        {
            if(ModelState.IsValid == false)
            {
                return BadRequest("Patrickpatrick");
            }

            await this._CustomerMan.RegisterUser(parameterCustomer);

            return Ok();
        }

       
    }
}
