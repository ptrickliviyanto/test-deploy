﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VUE.Entities.PostGre;
using VUE.ViewModels;

namespace VUE.Services
{
    public class LoginService
    {
        private readonly PostGreDbContext _AppDb;

        public LoginService(PostGreDbContext postGreDbContext)
        {
            this._AppDb = postGreDbContext;
        }
        public async Task<CustomerViewModel> GetData(CustomerFormLogin parameterForm)
        {
            var oneData = await this._AppDb.Customer.Select(Q => new CustomerViewModel
            {
                CustomerId = Q.CustomerId,
                CustomerUsername = Q.CustomerUsername,
                CustomerPassword = Q.CustomerPassword,
                CustomerRole = Q.CustomerRole,
            }).Where(Q => Q.CustomerUsername == parameterForm.CustomerUserNameForm
            ).FirstOrDefaultAsync();

            return oneData;
        }
    }
}
