﻿using Microsoft.EntityFrameworkCore;
using Minio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VUE.Entities.PostGre;
using VUE.ViewModels;

namespace VUE.Services
{
    public class ProductService
    {
        private readonly MinioClient _Minio;
        private readonly PostGreDbContext _AppDb;

        public ProductService(PostGreDbContext postGreDbContext, MinioClient minio)
        {
            this._Minio = minio;
            this._AppDb = postGreDbContext;
        }

        public async Task<ProductViewModel> GetProductbyId(int id)
        {
            var product = await this._AppDb.Product.Select(Q => new ProductViewModel {
                ProductId = Q.ProductId,
                ProductImage = Q.ProductImage,
                ProductName = Q.ProductName,
                ProductPrice = Q.ProductPrice
            }).FirstOrDefaultAsync(Q => Q.ProductId == id);
            return product;
        }
        
        public async Task<bool> UpdateProduct(int id, ProductInsertViewModel parameterProduct)
        {
            var blobID = Guid.NewGuid();
            var bucketName = "4-23-2019";
            var objectName = blobID.ToString();
            var contextType = parameterProduct.File.ContentType;
            var date = DateTime.Now;

            //product
            var oneData = await this._AppDb.Product.Where(Q => Q.ProductId == id).FirstOrDefaultAsync();
            var name = oneData.ProductImage.ToString();

            await this._Minio.RemoveObjectAsync(bucketName, name);
            using (var stream = parameterProduct.File.OpenReadStream())
            {
                await this._Minio.PutObjectAsync(bucketName, objectName, stream, stream.Length, contextType);
            }
            
            //blob
            var oneBlob = await this._AppDb.Blob.FirstOrDefaultAsync(Q => Q.BlobId == oneData.ProductImage);
            oneBlob.BlobId = blobID;
            oneBlob.BlobDate = date;
            oneBlob.BlobContentType = contextType;
            oneBlob.BlobName = parameterProduct.ProductInsertName;
            oneData.ProductName = parameterProduct.ProductInsertName;
            oneData.ProductPrice = parameterProduct.ProductPrice;
            oneData.ProductImage = blobID;

            await this._AppDb.SaveChangesAsync();
            return true;
        }

        public async Task<List<ProductViewModel>> GetAllProduct() {
            var allData = await this._AppDb.Product.Select(Q => new ProductViewModel {
                ProductId = Q.ProductId,
                ProductImage = Q.ProductImage,
                ProductName = Q.ProductName,
                ProductPrice = Q.ProductPrice
            }).ToListAsync();
            return allData;
        }

        public async Task<string> GetMinioImage(Guid id)
        {
            var bucketName = "4-23-2019";
            var imageName = id.ToString(); 
            var location = "us-east-1";

            bool found = await this._Minio.BucketExistsAsync(bucketName);
            if (!found)
            {
                await this._Minio.MakeBucketAsync(bucketName, location);
            }
            var expired = (int)TimeSpan.FromMinutes(15).TotalSeconds;


            var url = await this._Minio.PresignedGetObjectAsync(bucketName, imageName, expired);
            return url;
        }


        public async Task<bool> InsertProduct(ProductInsertViewModel parameterInsert)
        {
            var blobID = Guid.NewGuid();
            var date = DateTime.Now;

            var bucketName = "4-23-2019";
            var location = "us-east-1";
            var contextType = parameterInsert.File.ContentType;
            var objectName = blobID.ToString();

            bool found = await this._Minio.BucketExistsAsync(bucketName);
            //buat bucket / nunjuk bucket
            if (!found)
            {
                await this._Minio.MakeBucketAsync(bucketName,location);
            }

            //upload ke minio
            using (var stream = parameterInsert.File.OpenReadStream())
            {
                await this._Minio.PutObjectAsync(bucketName, objectName, stream, stream.Length, contextType);
            }

            this._AppDb.Add(new Blob
            {
                BlobId = blobID,
                BlobName = parameterInsert.ProductInsertName,
                BlobContentType = contextType,
                BlobDate = date
            });

            this._AppDb.Add(new Product
            {
                ProductImage = blobID,
                ProductName = parameterInsert.ProductInsertName,
                ProductPrice = parameterInsert.ProductPrice,
            });
            await this._AppDb.SaveChangesAsync();

            return true;
        }
    }
}
