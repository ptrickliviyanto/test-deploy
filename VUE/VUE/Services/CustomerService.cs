﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VUE.Entities.PostGre;
using VUE.ViewModels;

namespace VUE.Services
{
    public class CustomerService
    {
        private readonly PostGreDbContext _AppDb;

        public CustomerService(PostGreDbContext postGreDbContext)
        {
            this._AppDb = postGreDbContext;
        }

        public async Task<bool> RegisterUser(CustomerViewModel parameterCustomer)
        {
            var insert = new Customer
            {
                CustomerUsername = parameterCustomer.CustomerUsername,
                CustomerPassword = BCrypt.Net.BCrypt.HashPassword(parameterCustomer.CustomerPassword),
                CustomerRole = parameterCustomer.CustomerRole,
            };

            this._AppDb.Customer.Add(insert);
            await this._AppDb.SaveChangesAsync();
            return true;
        }
    }
}
