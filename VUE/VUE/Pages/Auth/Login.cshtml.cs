using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using VUE.Const;
using VUE.Services;
using VUE.ViewModels;

namespace VUE.Pages.Auth
{
    public class LoginModel : PageModel
    {
        private readonly LoginService _LoginMan;

        [BindProperty]
        public CustomerFormLogin Form { set; get; }

        public CustomerViewModel findData { set; get; }

        public LoginModel(LoginService loginService)
        {
            this._LoginMan = loginService;
        }
        public IActionResult OnGet()
        {
            if (User.Identity.IsAuthenticated)
            {
                return Redirect("../Index");
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl)
        {
            if (ModelState.IsValid == false)
            {
                return Page();
            }
            findData = await this._LoginMan.GetData(Form);

            if (findData == null)
            {
                ModelState.AddModelError("Form.CustomerPasswordForm", "Invalid Username & Password");
                return Page();
            }
            var validUsername = Form.CustomerUserNameForm == findData.CustomerUsername;
            var validPassword = BCrypt.Net.BCrypt.Verify(Form.CustomerPasswordForm, findData.CustomerPassword);

            var credential = validPassword && validUsername;

            if (credential == false)
            {
                ModelState.AddModelError("Form.CustomerPasswordForm", "Invalid Username & Password");
                return Page();
            }

            var claims = GenerateClaims();
            var persistance = new AuthenticationProperties
            {
                ExpiresUtc = DateTime.UtcNow.AddYears(1),
                IsPersistent = true
            };
            await HttpContext.SignInAsync(LoginAuthenticationSchemes.Cookie, claims, persistance);

            if (string.IsNullOrEmpty(returnUrl) == false)
            {
                return LocalRedirect(returnUrl);
            }

            var x = User.Identity.IsAuthenticated;

            return Redirect("../Index");
        }

        public ClaimsPrincipal GenerateClaims()
        {
            var claims = new ClaimsIdentity(LoginAuthenticationSchemes.Cookie);
            claims.AddClaim(new Claim(ClaimTypes.Name, findData.CustomerUsername));
            claims.AddClaim(new Claim(ClaimTypes.NameIdentifier, findData.CustomerUsername));
            claims.AddClaim(new Claim(ClaimTypes.Role, findData.CustomerRole));

            return new ClaimsPrincipal(claims);
        }
    }
}