using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using VUE.Const;
using VUE.Services;
using VUE.ViewModels;

namespace VUE.Pages.Admin
{
    [Authorize(Roles = RoleEnum.Admin)]
    public class InsertProductModel : PageModel
    {
        private readonly ProductService _ProductMan;

        public InsertProductModel(ProductService productService)
        {
            this._ProductMan = productService;
        }
        [BindProperty]
        public ProductInsertViewModel Form { get; set; }
        
        public void OnGet()
        {
        }
        public async Task<IActionResult> OnPostAsync()
        {
            if(ModelState.IsValid == false)
            {
                return Page();
            }

            var username = User.Identity.Name;
            var x = Form.File;
            var valid = await this._ProductMan.InsertProduct(Form);
            if(valid == false)
            {
                return BadRequest();
            }
            return Redirect("/Index");

        }
    }
}