using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using VUE.Services;
using VUE.ViewModels;

namespace VUE.Pages
{
    public class ViewProductModel : PageModel
    {
        private readonly ProductService _ProductMan;

        [BindProperty]
        public List<ProductViewModel> Products { get; set; }

        public ViewProductModel(ProductService productService)
        {
            this._ProductMan = productService;
        }
        public void OnGet()
        {
        }
    }
}