﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace VUE.PostGre
{
    public partial class PostGreDbContext : DbContext
    {

        public PostGreDbContext(DbContextOptions<PostGreDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Blob> Blob { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<Purchase> Purchase { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<Blob>(entity =>
            {
                entity.HasIndex(e => e.BlobId)
                    .HasName("UNIQUE_BlobID")
                    .IsUnique();

                entity.Property(e => e.BlobId).ValueGeneratedNever();
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.HasIndex(e => e.CustomerUsername)
                    .HasName("Unique_CustomerUsername")
                    .IsUnique();
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.HasIndex(e => e.ProductImage)
                    .HasName("UNIQUE_ProductImage")
                    .IsUnique();

                entity.HasOne(d => d.ProductImageNavigation)
                    .WithOne(p => p.Product)
                    .HasForeignKey<Product>(d => d.ProductImage)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProductImage");
            });
        }
    }
}
