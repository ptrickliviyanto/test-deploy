﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VUE.PostGre
{
    public partial class Purchase
    {
        [Column("PurchaseID")]
        public int PurchaseId { get; set; }
        [Column("PurchaseCustomerID")]
        public int PurchaseCustomerId { get; set; }
        [Column("PurchaseProductID")]
        public int PurchaseProductId { get; set; }
        [Column(TypeName = "date")]
        public DateTime PurchaseDate { get; set; }
    }
}
