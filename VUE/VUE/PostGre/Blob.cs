﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VUE.PostGre
{
    public partial class Blob
    {
        [Column("BlobID")]
        public Guid BlobId { get; set; }
        [Required]
        [Column(TypeName = "character varying")]
        public string BlobName { get; set; }
        [Required]
        [Column(TypeName = "character varying")]
        public string BlobContentType { get; set; }
        [Column(TypeName = "timestamp(6) without time zone")]
        public DateTime BlobDate { get; set; }

        [InverseProperty("ProductImageNavigation")]
        public virtual Product Product { get; set; }
    }
}
