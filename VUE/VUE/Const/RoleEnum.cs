﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VUE.Const
{
    public class RoleEnum
    {
        public const string User = "User";
        public const string Admin = "Admin";
    }
}
