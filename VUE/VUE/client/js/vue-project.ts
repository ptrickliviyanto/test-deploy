import Vue from 'vue';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import VeeValidate from 'vee-validate';
import Hello from './components/Hello.vue';
import InsertProduct from './components/InsertProduct.vue';
import Register from './components/Register.vue';

Vue.use(VeeValidate, {
    classes: true
});

Vue.component('fa', FontAwesomeIcon);

// components must be registered BEFORE the app root declaration
Vue.component('hello', Hello);
Vue.component('insert-product', InsertProduct);
Vue.component('register-customer', Register);

// bootstrap the Vue app from the root element <div id="app"></div>
new Vue().$mount('#app');
