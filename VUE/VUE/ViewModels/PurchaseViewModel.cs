﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace VUE.ViewModels
{
    public class PurchaseViewModel
    {
        public int PurchaseId { get; set; }
        public int PurchaseCustomerId { get; set; }
        public int PurchaseProductId { get; set; }
        public DateTime PurchaseDate { get; set; }
    }
}
