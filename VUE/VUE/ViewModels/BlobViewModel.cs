﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace VUE.ViewModels
{
    public class BlobViewModel
    {
        public Guid BlobId { get; set; }
        public string BlobName { get; set; }
        public string BlobContentType { get; set; }
        public DateTime BlobDate { get; set; }
    }
}
