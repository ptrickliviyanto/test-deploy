﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace VUE.ViewModels
{
    public class ProductViewModel
    {
        public int ProductId { get; set; }
        public Guid ProductImage { get; set; }
        public string ProductName { get; set; }
        public decimal ProductPrice { get; set; }
    }

    public class ProductInsertViewModel
    {
        public string ProductInsertName { get; set; }
        public int ProductPrice { get; set; }
        public IFormFile File { get; set; }
    }
}
