﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VUE.ViewModels
{
    public class CustomerViewModel
    {
        public int CustomerId { get; set; }
        [Display(Name = "Username")]
        public string CustomerUsername { get; set; }
        [Display(Name = "Password")]
        public string CustomerPassword { get; set; }
        public string CustomerRole { get; set; }
    }

    public class CustomerFormLogin
    {
        [Display(Name = "Username")]
        public string CustomerUserNameForm { get; set; }
        [Display(Name = "Password")]
        public string CustomerPasswordForm { get; set; }
    }
}
