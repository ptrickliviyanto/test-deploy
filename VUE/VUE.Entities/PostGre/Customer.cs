﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VUE.Entities.PostGre
{
    public partial class Customer
    {
        [Column("CustomerID")]
        public int CustomerId { get; set; }
        [Required]
        [StringLength(255)]
        public string CustomerUsername { get; set; }
        [Required]
        [StringLength(255)]
        public string CustomerPassword { get; set; }
        [Required]
        [StringLength(255)]
        public string CustomerRole { get; set; }
    }
}
