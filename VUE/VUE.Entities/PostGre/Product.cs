﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VUE.Entities.PostGre
{
    public partial class Product
    {
        [Column("ProductID")]
        public int ProductId { get; set; }
        public Guid ProductImage { get; set; }
        [Required]
        [StringLength(255)]
        public string ProductName { get; set; }
        [Column(TypeName = "money")]
        public decimal ProductPrice { get; set; }

        [ForeignKey("ProductImage")]
        [InverseProperty("Product")]
        public virtual Blob ProductImageNavigation { get; set; }
    }
}
